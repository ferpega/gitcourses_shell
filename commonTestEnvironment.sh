#!/bin/bash
# INSTALL_ON_DIR="`dirname \"$1\"`"

#set -x

if [ $# -eq 0 ]; then
  echo "*** Error..."
  echo "    Please, provide argument to where install/execute commands"
  echo "      And must be relative to script execution directory."
  echo "      Example: ./this_script.sh ../gitWsExecution  (must be previously created)"
  exit 1
fi

INSTALL_ON_DIR="$1"
echo "****************************************************"
echo "Installing software on: $INSTALL_ON_DIR"
echo "****************************************************"i
SUBMODULESDIR="ServerSubmodules"

SERVERDIR="$INSTALL_ON_DIR/Server"
SERVERSUBMODULESDIR="$INSTALL_ON_DIR/$SUBMODULESDIR"

CLIENTS_ROOT_DIR="$INSTALL_ON_DIR/Clients"
CLIENT_OFFICE="Office"
CLIENT_HOME="Home"
CLIENT_SUBMODULES="Submodules"
USUARIO="User"

rm -rf $SERVERDIR
rm -rf $SERVERSUBMODULESDIR
rm -rf $CLIENTS_ROOT_DIR

rm -rf $INSTALL_ON_DIR

mkdir $SERVERDIR
git init $SERVERDIR --bare
git init $SERVERSUBMODULESDIR --bare

function echoSection {
	echo " "
	echo $1
}

function clone {
	echoSection "Cloning Server to Client: $1 [$PWD]"
	git clone $SERVERDIR $CLIENTS_ROOT_DIR/$1
}

function clonesubmodules {
	echoSection "Cloning Server submodules to Client: $1 [$PWD]"
	git clone $SERVERSUBMODULESDIR $CLIENTS_ROOT_DIR/$1
}

function configureclient {
	echoSection "Configuring user on client: $1 [$PWD]"
	cd $1
	echo "   ---> on directory: [$PWD]"
	git config --local user.name "$USUARIO $1"
	git config --local user.email "$1@$USUARIO.com"
	cd ..
}

#git clone $SERVER:DIR $CLIENTS_ROOT_DIR/$CLIENT_OFFICE
clone $CLIENT_OFFICE
clone $CLIENT_HOME
clonesubmodules $CLIENT_SUBMODULES

cd $CLIENTS_ROOT_DIR/

configureclient $CLIENT_OFFICE
configureclient $CLIENT_HOME
configureclient $CLIENT_SUBMODULES

echoSection ">>>>>>>>>> End cloning and configuration"
echoSection ""

cd $CLIENT_SUBMODULES
echo "This is a file into SubModules from $CLIENT_SUBMODULES" >> InitialFileSubModules.txt
git add .
git commit -m "Initial submodules file"
git push origin master
cd ..

cd $CLIENT_OFFICE
echo "First line from $CLIENT_OFFICE" >> InitialOnOffice.txt
echo "Oh, no, my password is here." >> passwords.txt
git submodule add ../../$SERVERSUBMODULESDIR/
git add .
git commit -m "Initial file"
git push origin master
cd ..

cd $CLIENT_HOME
git pull origin master
git submodule init
git submodule update
echo "First line from $CLIENT_HOME" >> InitialOnHome.txt
git add .
echo -e "\r\n" >> InitialOnOffice.txt
echo "Second line from $CLIENT_HOME" >> InitialOnOffice.txt
git commit -am "Add and updates"
git push origin master
cd ..

cd $CLIENT_OFFICE
echoSection "*********> Adding commits to $CLIENT_OFFICE"
git pull origin master
git checkout -b Issue55
echo "First line" >> InitialOnIssue55.txt
git add .
git commit -m "First file from --- $CLIENT_OFFICE"
git push origin Issue55
git checkout master
cd ..

cd $CLIENT_HOME
echoSection "*********> Adding commits to $CLIENT_HOME"
git pull origin master
#read -p "Press enter to continue"
#git remote update
git fetch origin
git checkout -b Issue55 origin/Issue55
cd ..

cd $CLIENT_OFFICE
echoSection "*********> Adding commits to $CLIENT_OFFICE"
git pull origin master
git checkout -b Feature1
echo "$CLIENT_OFFICE: First line From $CLIENT_OFFICE" >> FeatureFromOffice.txt
git add .
git commit -m "Feature1: Initial Feature1 file"

echo -e "\r\n" >> InitialOnHome.txt
echo "$CLIENT_OFFICE: Second line from $CLIENT_OFFICE" >> InitialOnHome.txt
git commit -am "Feature1: Updated AGAIN"

echo -e "\r\n" >> FeatureFromOffice.txt
echo "$CLIENT_OFFICE: Second line from $CLIENT_OFFICE" >> FeatureFromOffice.txt
git commit -am "Feature1: Updated"

echo -e "\r\n" >> InitialOnHome.txt
echo "$CLIENT_OFFICE: Third line from $CLIENT_OFFICE" >> InitialOnHome.txt
echo -e "\r\n" >> FeatureFromOffice.txt
echo "$CLIENT_OFFICE: Third line from $CLIENT_OFFICE" >> FeatureFromOffice.txt
git commit -am "Feature1: Updated TWO files"
git push origin Feature1


git checkout Issue55
echo -e "\r\n" >> InitialOnIssue55.txt
echo "$CLIENT_OFFICE: Second line from $CLIENT_OFFICE" >> InitialOnIssue55.txt
git commit -am "Updated from --- $CLIENT_OFFICE"
git push origin Issue55

git checkout Feature1
echo -e "\r\n" >> FeatureFromOffice.txt
echo "$CLIENT_OFFICE: Fourth line from $CLIENT_OFFICE" >> FeatureFromOffice.txt
git commit -am "Feature1: Updated with a new line (fourth)"
git push origin Feature1
git checkout master
cd ..


cd $CLIENT_SUBMODULES
echoSection "*********> Adding a commit to submodules"
echo "This is another commit on submodules repo" >> SecondFileSubmodules.txt
git add .
git commit -m "SecondFile on Submodules Repo"
git push origin master
cd ..
